#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include "../libft/includes/libft.h"
# include <stdlib.h>
# include <stdio.h>
# define A	1
# define B	2
# define AB	3

typedef struct	s_list
{
	int					value;

	struct s_list		*next;
	struct s_list		*prev;
}				t_list;

typedef struct	s_env
{
	t_list				*a;
	t_list				*b;

	int					n_elems;
}				t_env;


/*
**	list.c
*/
void	add_to_list(t_list **first, int value);


/*
**	moves.c
*/
void	rotate_a(t_env *e);
void	rotate_b(t_env *e);
void	rotate(t_env *e, char list);

void	swap_a(t_env *e);
void	swap_b(t_env *e);
void	swap(t_env *e, char list);

void	push_a(t_env *e);
void	push_b(t_env *e);
void	pushh(t_env *e, char list);

/*
**	sort.c
*/
void	sort(t_env *e);

/*
**	parser.c
*/
void	parse(t_env *e, int argc, char **argv);

#endif
NAME = push_swap
CC = gcc
FLAGS = -Wall -Werror -Wextra

LIBFT_A =		../libft/libft.a

SCRS =			srcs/parser.c\
				srcs/list.c\
				srcs/push_swap.c\
				srcs/moves.c\
				srcs/sort.c

OBJS =			$(SCRS:%.c=%.o)

all: $(NAME)

$(NAME): $(OBJS)
	gcc -o $(NAME) $(FLAGS) $(SCRS) $(LIBFT_A)

%.o: %.c
	gcc -o $@ -c $< $(FLAGS)

clean:
	/bin/rm -f $(OBJS)

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all

#include "../push_swap.h"

void	add_to_list(t_list **first, int value)
{
	t_list		*ptr;
	t_list		*new_elem;

	new_elem = (t_list *)malloc(sizeof(t_list));
	if (new_elem == NULL)
		ft_usage("Malloc error");
	new_elem->value = value;
	ptr = *first;
	while (ptr->next != *first)
		ptr = ptr->next;
	ptr->next = new_elem;

	new_elem->prev = ptr;
	new_elem->next = *first;
	(*first)->prev = new_elem;
}
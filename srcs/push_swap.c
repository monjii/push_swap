#include "../push_swap.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////IDEE DE DEPART//- diviser pour régner///////////////////////////////
/////////////////////////////////utiliser la médiane pour//////////////////////
/////////////////////////////////répartir le tri en deux///////////////////////
/////////////////////////////////sous-listes à trier, puis combiner////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void	display_lists(t_env *e)
{
	t_list	*ptr = e->a->next;
	printf("l_a -> ");
	while(ptr != e->a)
	{
		printf("%d ", ptr->value);
		ptr = ptr->next;
	}
	printf("\n");
	ptr = e->b->next;
	printf("l_b -> ");
	while(ptr != e->b)
	{
		printf("%d ", ptr->value);
		ptr = ptr->next;
	}
	printf("\n");
}

int		main(int argc, char **argv)
{
	t_env	*e;

	e = (t_env *)malloc(sizeof(t_env));
	e->a = (t_list *)malloc(sizeof(t_list));
	e->a->next = e->a;
	e->a->prev = e->a;
	e->b = (t_list *)malloc(sizeof(t_list));
	e->b->next = e->b;
	e->b->prev = e->b;
	e->n_elems = 0;
	parse(e, argc, argv);
	// printf("n_elems -> %d\n", e->n_elems);
	sort(e);
	// push_b(e);
	// display_lists(e);
	// push_a(e);
	// display_lists(e);
//	printf("Push Swap\n");
	return (0);
}
#include "../push_swap.h"

void	sort_tab(int *tab, unsigned int size)
{
	unsigned int	i;
	int				tmp;

	i = 0;
	while(i < size - 1)
	{
		if (tab[i] > tab[i + 1])
		{
			tmp = tab[i];
			tab[i] = tab[i + 1];
			tab[i + 1] = tmp;
			i = 0;
		}
		else
			i++;
	}
}

int		*copy_values(t_env *e)
{
	int		i;
	int		*tab;
	t_list	*ptr;

	tab = (int*)malloc(sizeof(int) * e->n_elems);

	i = 0;
	ptr = e->a->next;
	while (ptr != e->a)
	{
		tab[i] = ptr->value;
		ptr = ptr->next;
		i++;
	}
	return (tab);
}

int		get_median(t_env *e)
{
	int		median;
	int		*values;

	values = copy_values(e);
	sort_tab(values, e->n_elems);
	if (e->n_elems % 2 == 0)	//NOMBRE PAIR DE VALEURS
		median = values[e->n_elems / 2];	// [1] [3] *[7]* [8]
	else						//NOMBRE IMPAIR DE VALEURS
		median = values[e->n_elems / 2];	// [1] [3] *[4]* [7] [8]
	return (median);
}

int		is_sort(t_env *e, t_list *root)
{
	t_list	*ptr;

	ptr = root->next;
	while (ptr->next != root)
	{
		if (root == e->a && ptr->value > ptr->next->value)
			return (0);
		else if (root == e->b && ptr->value < ptr->next->value)
			return (0);
		ptr = ptr->next;
	}
	return (1);
}

void	sort(t_env *e)
{
	int		push;
	int		median;

	median = get_median(e);
	// printf("is sort ? %d\n", is_sort(e, e->A));
	// printf("median -> %d\n", median);
	// swap(e, LIST_A);
	push = 0;
	if (!is_sort(e, e->a))
	{
		while (push < e->n_elems / 2)
		{
			if (e->a->next->value < median)
			{
				pushh(e, B);
				push++;
			}
			else
				rotate(e, A);
		}
	}

	// while (!is_sort(e->A))
	// {

	// }
}
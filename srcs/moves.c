#include "../push_swap.h"

// void	pushh(t_env *e, char list)
// {
// 	if (list == LIST_A)
// 		push_a(e);
// 	else if (list == LIST_B)
// 		push_b(e);
// 	else
// 	{
// 		push_a(e);
// 		push_b(e);
// 	}
// }
void	rotate_a(t_env *e)
{
	t_list	*tail;
	t_list	*head;

	tail = e->a->prev;
	head = e->a->next;
	tail->next = head;
	head->prev = tail;
	e->a->next = head->next;
	head->next->prev = e->a;
	e->a->prev = head;
	head->next = e->a;
}

void	rotate_b(t_env *e)
{
	t_list	*tail;
	t_list	*head;

	tail = e->b->prev;
	head = e->b->next;
	tail->next = head;
	head->prev = tail;
	e->b->next = head->next;
	head->next->prev = e->b;
	e->b->prev = head;
	head->next = e->b;
}

void	rotate(t_env *e, char list)
{
	if (list == A)
		rotate_a(e);
	else if (list == B)
		rotate_b(e);
	else
	{
		rotate_a(e);
		rotate_b(e);
	}
}

void	push_a(t_env *e)
{
	t_list		*new_elem;

	if (e->b->next != e->b)
	{
		new_elem = (t_list *)malloc(sizeof(t_list));
		if (new_elem == NULL)
			ft_usage("Malloc error");
		new_elem->value = e->b->next->value;
		e->a->next->prev = new_elem;
		new_elem->next = e->a->next;
		new_elem->prev = e->a;
		e->a->next = new_elem;

		e->b->next = e->b->next->next;
	}
}

void	push_b(t_env *e)
{
	t_list		*new_elem;

	if (e->a->next != e->a)
	{
		new_elem = (t_list *)malloc(sizeof(t_list));
		if (new_elem == NULL)
			ft_usage("Malloc error");
		new_elem->value = e->a->next->value;
		e->b->next->prev = new_elem;
		new_elem->next = e->b->next;
		new_elem->prev = e->b;
		e->b->next = new_elem;

		e->a->next = e->a->next->next;
	}
}

void	pushh(t_env *e, char list)
{
	if (list == A)
		push_a(e);
	else if (list == B)
		push_b(e);
	else
	{
		push_a(e);
		push_b(e);
	}
}

void	swap_a(t_env *e)
{
	t_list	*one;
	t_list	*two;
	t_list	*three;

	one = e->a->next;
	two = e->a->next->next;
	three = e->a->next->next->next;

	three->prev = one;
	one->next = three;
	one->prev = two;
	two->next = one;
	two->prev = e->a;
	e->a->next = two;
}

void	swap_b(t_env *e)
{
	t_list	*one;
	t_list	*two;
	t_list	*three;

	one = e->b->next;
	two = e->b->next->next;
	three = e->b->next->next->next;

	three->prev = one;
	one->next = three;
	one->prev = two;
	two->next = one;
	two->prev = e->b;
	e->b->next = two;
}

void	swap(t_env *e, char list)
{
	if (list == A)
		swap_a(e);
	else if (list == B)
		swap_b(e);
	else
	{
		swap_a(e);
		swap_b(e);
	}
}
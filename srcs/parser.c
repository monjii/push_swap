#include "../push_swap.h"

void	check_params(char *str)
{
	if (((*str == '+' || *str == '-') && *(str + 1) != '\0') || ft_isdigit(*str))
		str++;
	else
		ft_usage("Error: invalid character found");
	while(*str)
	{
		if (!ft_isdigit(*str))
			ft_usage("Error: invalid character found");
		str++;
	}
}

int		value_exists(t_env *e, int value)
{
	t_list	*ptr;

	ptr = e->a->next;
	while (ptr != e->a)
	{
		if (ptr->value == value)
			return (1);
		ptr = ptr->next;
	}
	return (0);
}

void	parse(t_env *e, int argc, char **argv)
{
	int		i;
	int		value;

	if (argc < 2)
		ft_usage("Error: Not enought params");
	i = 1;
	while (i < argc)
	{
		check_params(argv[i]);
		value = atoi(argv[i]);
		if (value_exists(e, value))
			ft_usage("Error: one value was entered 2 times");
		add_to_list(&e->a, value);
		e->n_elems += 1;
		i++;
	}
}
